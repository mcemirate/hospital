package com.emirate.hospital.components;

import com.emirate.hospital.leafs.Component;

import java.util.ArrayList;

public class CompositeUntersuchung extends Component {
    private ArrayList<Component> children;

    public CompositeUntersuchung() {
        children = new ArrayList<>();
    }

    @Override
    public void operation() {
        System.out.println("Untersuchung wird durchgeführt");
    }

    @Override
    public int getPrize() {
        int sum = 0;
        for (int i = 0; i < children.size(); i++) {
            sum += children.get(i).getPrize();
        }
        return sum;
    }

    public void add(Component children) {
        this.children.add(children);
    }

    public void remove(int position) throws IndexOutOfBoundsException{
        this.children.remove(position);
    }

    public Component getChild(int position) throws IndexOutOfBoundsException{
        return this.children.get(position);
    }

    public int getChildrenSize() {
        return this.children.size();
    }
}
