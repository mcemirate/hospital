package com.emirate.hospital;

import com.emirate.hospital.components.CompositeUntersuchung;
import com.emirate.hospital.leafs.BlutdruckMessen;
import com.emirate.hospital.leafs.FieberMessen;

public class Main {
    public static void main(String[] args) {
        CompositeUntersuchung uMain = new CompositeUntersuchung();
        uMain.add(new FieberMessen());

        CompositeUntersuchung kleineUntersuchung = new CompositeUntersuchung();
        kleineUntersuchung.add(new FieberMessen());
        kleineUntersuchung.add(new BlutdruckMessen());
        uMain.add(kleineUntersuchung);

        System.out.println("Der Preis fuer die gesamte Untesuchung ist: " + uMain.getPrize());

        CompositeUntersuchung miniUntersuchung = new CompositeUntersuchung();
        miniUntersuchung.add(new BlutdruckMessen());
        miniUntersuchung.add(new FieberMessen());
        kleineUntersuchung.add(miniUntersuchung);

        System.out.println("Der Preis fuer die gesamte Untesuchung ist: " + uMain.getPrize());
    }
}
