package com.emirate.hospital.leafs;

public abstract class Component {
    abstract public void operation();
    abstract public int getPrize();
}
